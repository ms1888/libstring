CFLAGS=-O2
LDFLAGS=-Wl,-s
SRCS=chrcasecmp.c cpu_features.c memccpy.c memchr.c memcmp.c memcpy.c memmove.c memmem.c memset.c rawmemchr.c strcasecmp.c strcat.c strchr.c strcmp.c strcpy.c strcspn.c strdup.c strlen.c strncasecmp.c strncat.c strncmp.c strncpy.c strndup.c strnlen.c strpbrk.c strrchr.c strspn.c strstr.c strtok.c
OBJS=$(SRCS:.c=.o)

libstring.so: $(OBJS)
	$(CC) $(LDFLAGS) -shared -fPIC -Wl,-soname,$@ $(OBJS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) -fopenmp -D_XOPEN_SOURCE=700 -c -fPIC $< -o $@

chrcasecmp.o: case_folding.h

case_folding.h: CaseFolding.txt
	sed 's:#.*::' $< | sed '/^$$/d' | sed '/.*; T; .*;/d' | cut -d\; -f1,3 | sed 's:\<\([^ ]*\)\>:0X\1,:g' | sed 's:^\(.*\); \(.*\)$$:ENTRY(\1 ((int32_t[]){\2 0})):' > $@

clean:
	$(RM) libstring.so case_folding.h $(OBJS)
