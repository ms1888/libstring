/*
 * Copyright (c) 2020, 2021, Matija Skala <mskala@gmx.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the author nor the
 *     names of contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Matija Skala <mskala@gmx.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Matija Skala <mskala@gmx.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <immintrin.h>

#include "cpu_features.h"
#include "helpers.h"
#include "internal.h"

static int32_t *i32memchr_fallback(const int32_t *haystack, int32_t needle, size_t size) {
	if (size >= sizeof(size_t)/2)
		while ((size_t)haystack % sizeof(size_t)) {
			if (*haystack == needle)
				return (void*)haystack;
			haystack++;
			size--;
		}
	size_t lowbits = SIZE_MAX / UINT32_MAX;
	size_t highbits = lowbits * (size_t)INT32_MIN;
	size_t repeated_n = lowbits * needle;
	while (size >= sizeof(size_t)/2) {
		size_t m1 = *(const size_t*)haystack ^ repeated_n;
		size_t m2 = *((const size_t*)haystack+1) ^ repeated_n;
		if ((((m1-lowbits) & ~m1) | ((m2-lowbits) & ~m2)) & highbits) {
			while (*haystack != needle)
				haystack++;
			return (void*)haystack;
		}
		haystack += sizeof(size_t)/2;
		size -= sizeof(size_t)/2;
	}
	while (size) {
		if (*haystack == needle)
			return (void*)haystack;
		haystack++;
		size--;
	}
	return NULL;
}

__attribute__((__target__("sse2")))
static int32_t *i32memchr_sse2(const int32_t *haystack, int32_t needle, size_t size) {
	if (size >= 4) {
		while ((size_t)haystack % 16) {
			if (*haystack == needle)
				return (void*)haystack;
			haystack++;
			size--;
		}
	}
	__m128i vn = _mm_set1_epi32(needle);
	while (size >= 16) {
		__m128i a = _mm_load_si128((const __m128i*)haystack);
		__m128i b = _mm_load_si128((const __m128i*)haystack+1);
		__m128i eqa = _mm_cmpeq_epi32(vn, a);
		__m128i eqb = _mm_cmpeq_epi32(vn, b);
		__m128i or1 = _mm_or_si128(eqa, eqb);

		__m128i c = _mm_load_si128((const __m128i*)haystack+2);
		__m128i d = _mm_load_si128((const __m128i*)haystack+3);
		__m128i eqc = _mm_cmpeq_epi32(vn, c);
		__m128i eqd = _mm_cmpeq_epi32(vn, d);
		__m128i or2 = _mm_or_si128(eqc, eqd);

		__m128i or3 = _mm_or_si128(or1, or2);
		if (_mm_movemask_epi8(or3)) {
			int mask;
			if ((mask = _mm_movemask_epi8(eqa)))
				return (void*)(haystack + trailing_zeros(mask)/4);
			if ((mask = _mm_movemask_epi8(eqb)))
				return (void*)(haystack + 4 + trailing_zeros(mask)/4);
			if ((mask = _mm_movemask_epi8(eqc)))
				return (void*)(haystack + 8 + trailing_zeros(mask)/4);
			if ((mask = _mm_movemask_epi8(eqd)))
				return (void*)(haystack + 12 + trailing_zeros(mask)/4);
			return NULL;
		}
		haystack += 16;
		size -= 16;
	}
	while (size >= 4) {
		__m128i x = _mm_load_si128((void*)haystack);
		__m128i eq = _mm_cmpeq_epi32(x, vn);
		int mask = _mm_movemask_epi8(eq);
		if (mask)
			return (void*)(haystack + trailing_zeros(mask) / 4);
		haystack += 4;
		size -= 4;
	}
	while (size) {
		if (*haystack == needle)
			return (void*)haystack;
		haystack++;
		size--;
	}
	return NULL;
}

__attribute__((__target__("avx2")))
static int32_t *i32memchr_avx2(const int32_t *haystack, int32_t needle, size_t size) {
	if (size >= 4) {
		while ((size_t)haystack % 32) {
			if (*haystack == needle)
				return (void*)haystack;
			haystack++;
			size--;
		}
	}
	__m256i vn = _mm256_set1_epi32(needle);
	while (size >= 32) {
		__m256i a = _mm256_load_si256((const __m256i*)haystack);
		__m256i b = _mm256_load_si256((const __m256i*)haystack+1);
		__m256i eqa = _mm256_cmpeq_epi32(vn, a);
		__m256i eqb = _mm256_cmpeq_epi32(vn, b);
		__m256i or1 = _mm256_or_si256(eqa, eqb);

		__m256i c = _mm256_load_si256((const __m256i*)haystack+2);
		__m256i d = _mm256_load_si256((const __m256i*)haystack+3);
		__m256i eqc = _mm256_cmpeq_epi32(vn, c);
		__m256i eqd = _mm256_cmpeq_epi32(vn, d);
		__m256i or2 = _mm256_or_si256(eqc, eqd);

		__m256i or3 = _mm256_or_si256(or1, or2);
		if (_mm256_movemask_epi8(or3)) {
			int mask;
			if ((mask = _mm256_movemask_epi8(eqa)))
				return (void*)(haystack + trailing_zeros(mask)/4);
			if ((mask = _mm256_movemask_epi8(eqb)))
				return (void*)(haystack + 8 + trailing_zeros(mask)/4);
			if ((mask = _mm256_movemask_epi8(eqc)))
				return (void*)(haystack + 16 + trailing_zeros(mask)/4);
			if ((mask = _mm256_movemask_epi8(eqd)))
				return (void*)(haystack + 24 + trailing_zeros(mask)/4);
			return NULL;
		}
		haystack += 32;
		size -= 32;
	}
	__m128i v16n = _mm_set1_epi32(needle);
	while (size >= 4) {
		__m128i x = _mm_load_si128((void*)haystack);
		__m128i eq = _mm_cmpeq_epi32(x, v16n);
		int mask = _mm_movemask_epi8(eq);
		if (mask)
			return (void*)(haystack + trailing_zeros(mask)/4);
		haystack += 4;
		size -= 4;
	}
	while (size) {
		if (*haystack == needle)
			return (void*)haystack;
		haystack++;
		size--;
	}
	return NULL;
}

static int32_t *i32memchr_auto(const int32_t *haystack, int32_t c, size_t n);

static int32_t *(*i32memchr_impl)(const int32_t *haystack, int32_t c, size_t n) = i32memchr_auto;

static int32_t *i32memchr_auto(const int32_t *haystack, int32_t c, size_t n) {
	if (has_avx2())
		i32memchr_impl = i32memchr_avx2;
	else if (has_sse2())
		i32memchr_impl = i32memchr_sse2;
	else
		i32memchr_impl = i32memchr_fallback;
	return i32memchr_impl(haystack, c, n);
}

int32_t *i32memchr(const int32_t *haystack, int32_t c, size_t n) {
	return i32memchr_impl(haystack, c, n);
}

int32_t case_folding1[] = {
#define ENTRY(A,B) A,
#include "case_folding.h"
#undef ENTRY
};

int32_t *case_folding2[] = {
#define ENTRY(A,B) B,
#include "case_folding.h"
#undef ENTRY
};

__attribute__((visibility("hidden")))
int chrncasecmp(const char *s1, size_t *n1, const char *s2, size_t *n2) {
	if ((*s1|32) >= 'a' && (*s2|32) >= 'a' && (*s1|32) <= 'z' && (*s2|32) <= 'z') {
		*n1 = 1;
		*n2 = 1;
		return (*s1|32) - (*s2|32);
	}
	if ((unsigned char)*s1 <= 0x7f && (unsigned char)*s2 <= 0x7f) {
		*n1 = 1;
		*n2 = 1;
		return (int)(unsigned char)*s1 - (int)(unsigned char)*s2;
	}
	int c1l = 1;
	int c2l = 1;
	int32_t c1 = (unsigned char)*s1;
	int32_t c2 = (unsigned char)*s2;
	{
		if ((*s1 & 0xc0) == 0xc0) {
			while (*s1 >= 0xff << (7-c1l))
				c1l++;
			if (c1l > (int32_t)*n1) {
				*n1 = 1;
				*n2 = 1;
				return (int)(unsigned char)*s1 - (int)(unsigned char)*s2;
			}
			c1 &= 0xfe >> c1l;
			for (int j = 1; j < c1l; j++) {
				if ((s1[j] & 0xc0) != 0x80) {
					*n1 = 1;
					*n2 = 1;
					return (int)(unsigned char)*s1 - (int)(unsigned char)*s2;
				}
				c1 <<= 6;
				c1 |= s1[j] & 0x3f;
			}
		}
		if ((*s2 & 0xc0) == 0xc0) {
			while (*s2 >= 0xff << (7-c2l))
				c2l++;
			if (c2l > (int32_t)*n2) {
				*n1 = 1;
				*n2 = 1;
				return (int)(unsigned char)*s1 - (int)(unsigned char)*s2;
			}
			c2 &= 0xfe >> c2l;
			for (int j = 1; j < c2l; j++) {
				if ((s2[j] & 0xc0) != 0x80) {
					*n1 = 1;
					*n2 = 1;
					return (int)(unsigned char)*s1 - (int)(unsigned char)*s2;
				}
				c2 <<= 6;
				c2 |= s2[j] & 0x3f;
			}
		}
	}
	if (c1 == c2) {
		*n1 = c1l;
		*n2 = c2l;
		return 0;
	}
	int32_t *entry_ptr = i32memchr(case_folding1, c1, sizeof case_folding1/sizeof *case_folding1);
	if (!entry_ptr) {
		entry_ptr = i32memchr(case_folding1, c2, sizeof case_folding1/sizeof *case_folding1);
		if (!entry_ptr)
			return (c1 > c2) - (c1 < c2);
		size_t offset = entry_ptr - case_folding1;
		while (offset < sizeof case_folding1/sizeof *case_folding1 && entry_ptr[offset] == c2 && case_folding2[offset][0] != c1)
			offset++;
		if (offset >= sizeof case_folding1/sizeof *case_folding1 || entry_ptr[offset] != c2)
			return (c1 > c2) - (c1 < c2);
		for (int i = 1; case_folding2[offset][i]; i++) {
			int c3l = 1;
			int32_t c3 = (unsigned char)s1[c1l];
			if ((s1[c1l] & 0xc0) == 0xc0) {
				while (s1[c1l] >= 0xff << (7-c3l))
					c3l++;
				if (c1l + c3l > (int32_t)*n1) {
					return (c1 > c2) - (c1 < c2);
				}
				c3 &= 0xfe >> c3l;
				for (int j = 1; j < c3l; j++) {
					if ((s1[c1l+j] & 0xc0) != 0x80)
						return (c1 > c2) - (c1 < c2);
					c3 <<= 6;
					c3 |= s1[c1l+j] & 0x3f;
				}
			}
			if (case_folding2[offset][i] != c3)
				return (c1 > c2) - (c1 < c2);
			c1l += c3l;
		}
		*n1 = c1l;
		*n2 = c2l;
		return 0;
	}
	size_t offset = entry_ptr - case_folding1;
	while (offset < sizeof case_folding1/sizeof *case_folding1 && entry_ptr[offset] == c1 && case_folding2[offset][0] != c2)
		offset++;
	if (offset >= sizeof case_folding1/sizeof *case_folding1 || entry_ptr[offset] != c1)
		return (c1 > c2) - (c1 < c2);
	for (int i = 1; case_folding2[offset][i]; i++) {
		int c3l = 1;
		int32_t c3 = (unsigned char)s2[c2l];
		if ((s2[c2l] & 0xc0) == 0xc0) {
			while (s2[c2l] >= 0xff << (7-c3l))
				c3l++;
			if (c2l + c3l > (int32_t)*n2)
				return (c1 > c2) - (c1 < c2);
			c3 &= 0xfe >> c3l;
			for (int j = 1; j < c3l; j++) {
				if ((s2[c2l+j] & 0xc0) != 0x80)
					return (c1 > c2) - (c1 < c2);
				c3 <<= 6;
				c3 |= s2[c2l+j] & 0x3f;
			}
		}
		if (case_folding2[offset][i] != c3)
			return (c1 > c2) - (c1 < c2);
		c2l += c3l;
	}
	*n1 = c1l;
	*n2 = c2l;
	return 0;
}
